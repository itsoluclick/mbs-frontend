module.exports = {
  parser: 'babel-eslint',
  root: true,
  ignorePatterns: [
    '**/functions/**',
    '**/.next/**',
    '**/out/**',
    '**/node_modules/**',
  ],
  env: {
    browser: true,
    jest: true,
    node: true,
  },
  plugins: ['react', 'prettier', 'react-hooks', 'simple-import-sort'],
  extends: [
    'eslint:recommended',
    'plugin:prettier/recommended',
    'plugin:react/recommended',
  ],
  overrides: [
    {
      files: ['*.js', '*.jsx', '*.ts'],
    },
  ],
  rules: {
    'prettier/prettier': ['error', { singleQuote: true, trailingComma: 'es5' }],
    'react/prop-types': 'off',
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    'no-nested-ternary': 'error',
    'simple-import-sort/imports': [
      'error',
      {
        groups: [
          // Default Groups
          // Side effect imports.
          ['^\\u0000'],
          // Packages.
          // Things that start with a letter (or digit or underscore), or `@` followed by a letter.
          ['^@?\\w'],
          // Absolute imports and other imports such as Vue-style `@/foo`.
          // Anything that does not start with a dot.
          ['^[^.]'],
          // Relative imports.
          // Anything that starts with a dot.
          ['^\\.'],
          // Custom Groups
          ['^src/'],
        ],
      },
    ],
  },
  settings: {
    react: {
      pragma: 'React', // Pragma to use, default to "React"
      version: 'detect', // React version. "detect" automatically picks the version you have installed.
    },
  },
};
