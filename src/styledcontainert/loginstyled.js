import { Field, Form } from 'formik';
import styled, { createGlobalStyle } from 'styled-components';

var scolor = 'rgb(155, 149, 160)';

// globalStyles.js
/*style de body  */
export const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    background: #002d42;
    font-family: Open-Sans, Helvetica, Sans-Serif;
      }


`;
/*style de Container  */
export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;
export const H2 = styled.h2`
  color: ${scolor};
  border: none;
  margin-bottom: 30px;
  text-align: center;
  margin-right: 20px;
  font-family: console;
`;
/*style de  image */

export const Imagens = styled.div`
  overflow: hidden;
  top: calc(-100px / 2);
  left: calc(50% - 50px);
  position: absolute;
  border-radius: 50px;
  width: 100px;
  height: 100px;
`;

/*style de  div */
export const Div = styled.div`
  position: relative;
  height: 48px;
  margin-bottom: 1.5rem;
`;
/*style de  from */
export const Form1 = styled(Form)`
  width: 310px;
  height: 450px;
  padding: 80px 40px;
  border-radius: 10px;
  box-sizing: border-box;
  background: #fff;
  padding: 4rem 2rem;
  position: relative;
  box-shadow: 0 10px 25px rgb(79, 76, 50);
  flex-direction: column;
  justify-content: center;
  align-items: center;
  display: flex;
  overflow: visible;
  @media screen and(max-width: 900px) {
    transform: 0.8s all ease;
  }
`;
//icon
export const Icon = styled.i`
  color: ${scolor};
  position: absolute;
  font-size: 21px;
  top: 0.3rem;
  margin-top: 0.1rem;
  padding-left: -0rem;
  &:focus,
  &:active {
    color: #5eaefe;
    outline: none;
  }
`;
/*style de  input */
export const Fields = styled(Field)`
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  box-sizing: border-box;
  border: none;
  border-bottom: 0.5px solid #bdbdbd;
  font-size: 1.25em;
  padding-left: 1.25em;
  padding-top: 0.25em;
  width: 100%;
  min-width: 11em;
  font-family: console;
  &::placeholder {
    color: ${scolor};
    font-family: console;
  }

  &::not(::last-of-type) {
    border-bottom: 1.5px solid rgba(200, 200, 200, 0.4);
  }
  &:focus {
    outline: none;
    border-bottom: 2px solid rgba(200, 200, 200, 0.8);
  }
  :focus {
    border-color: #5eaefe;
    outline: none;
  }
  & Icon {
    &:active {
      color: #5eaefe;
      outline: none;
    }
  }
`;

/*style de  button */
export const Butun = styled.button`
  width: 100%;
  margin-bottom: 20px;
  border: none;
  outline: none;
  font-size: 22px;
  height: 35px;
  color: #fff;
  background-color: #0094ff;
  cursor: pointer;
  transition: 0.3s ease-in-out;
  border-radius: 16px;
  margin-top: 100px;
  font-weight: bold;
  min-width: 11em;

  :hover {
    background-color: #264b65;
    cursor: pointer;
  }
`;
/*style de  error */
export const Error = styled.div`
  color: red;
  display: block;
  font-family: console;
  font-size: 16px;
  text-align: left;
  padding: 0.3rem 0.75rem;
  margin-top: 0.1rem;
  white-space: pre-line;
  padding-left: 1.6em;
  &:focus,
  &:active {
    box-shadow: rgb(244, 129, 116) 0px 0px 2px 1px,
      rgb(251, 178, 174) 0px 0px 0px 3px;
    border: 1px solid rgb(191, 49, 12);
    outline: none;
  }
`;
