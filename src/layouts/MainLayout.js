import { useRouter } from 'next/router';
import React, { useEffect } from 'react';

import Cards from '../components/cards/CardLayoust';
import { Container } from '../components/cards/Layoust';
import Sidebar from '../components/Sidebar/Sidebar';

const MainLayout = ({ children }) => {
  const router = useRouter();
  useEffect(() => {
    const handleRouteChange = (url) => console.log(url);
    router.events.on('routeChangeStart', handleRouteChange);
    return () => {
      router.events.off('routeChangeStart', handleRouteChange);
    };
  }, []);
  return (
    <>
      <Sidebar />
      <Container>
      <Cards></Cards>
      </Container>
     
      {children}
    </>
  );
};

export default MainLayout;
