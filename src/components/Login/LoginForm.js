import { Formik } from 'formik';
import Image from 'next/image';
import React from 'react';
import { FaLock, FaUserAlt } from 'react-icons/fa';
import * as Yup from 'yup';

import {
  Butun,
  Container,
  Div,
  Error,
  Fields,
  Form1,
  GlobalStyle,
  H2,
  Icon,
  Imagens,
} from './styled/loginstyled';

function LoginForm() {
  const initalValidation = {
    txtusuario: '',
    txtpassword: '',
  };
  const validaSchema = Yup.object().shape({
    txtpassword: Yup.string()
      .required('Password required...*')
      .max(8, 'carateres maximo 8.')
      .matches(/(?=.*[0-9])/, 'Password must contain a number.'),
    txtusuario: Yup.string()
      .required('Usuario required...*')
      .max(50, 'carateres maximo 50.'),
  });
  return (
    <Formik
      initialValues={initalValidation}
      validationSchema={validaSchema}
      validateOnMount={true}
      onSubmit={(values, actions) => {
        console.log(values.txtpassword);

        setTimeout(() => {
          actions.setSubmitting(false);
        }, 500);
        actions.resetForm({});
      }}
    >
      {({ handleSubmit, errors, touched, values }) => {
        return (
          <Container>
            <GlobalStyle />

            <Form1 onSubmit={handleSubmit}>
              <Imagens>
                <Image width={100} height={100} src="/logo.jpg"></Image>
              </Imagens>
              <H2 className="DIV">Login MBS</H2>
              <Div>
                <Icon>
                  <FaUserAlt />
                </Icon>
                <Fields
                  className={'Fields'}
                  type="text"
                  name="txtusuario"
                  id="txtusuario"
                  placeholder={'Usuario'}
                  autoComplete={'off'}
                  maxLength={'50'}
                  value={values.txtusuario}
                />{' '}
                {errors.txtusuario && touched.txtusuario && (
                  <Error>{errors.txtusuario}</Error>
                )}
              </Div>
              <Div>
                <Icon>
                  <FaLock />
                </Icon>
                <Fields
                  className={'Fields'}
                  type="password"
                  name="txtpassword"
                  id="txtpassword"
                  placeholder={'Password'}
                  autoComplete={'off'}
                  maxLength={'8'}
                  value={values.txtpassword}
                />
                {errors.txtpassword && touched.txtpassword && (
                  <Error>{errors.txtpassword}</Error>
                )}
              </Div>
              <Butun type="submit">Sign In</Butun>
            </Form1>
          </Container>
        );
      }}
    </Formik>
  );
}

export default LoginForm;
