import styled from 'styled-components';


export const Header = styled.h3`
  color: whitesmoke;
`;
export const Nav = styled.nav`
  background: #027196;
  height: 50px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  box-sizing: border-box;
  border-bottom: #ff8000 1px solid;
  text-align: center;
  position: fixed;
  z-index: 100;
  width: 100%;
  @media (max-width: 960px) {
    width: 100vw;
  }
`;

export const NavIcon = styled.i`
  margin-left: 0.5rem;
  font-size: 15px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  position: relative;
  margin-left: 20px;
  transition: 350ms;
  z-index: 10;
  color: white;
  &:hover {
    color: #091b20;
    border: #ff8000 2px solid;
    cursor: pointer;
    font-size: 28px;
  }
`;
export const Imagens = styled.div`
  overflow: hidden;
  position: absolute;
  border-radius: 100px;
  width: 40px;
  height: 40px;
  margin-left: auto;
  margin-right: 50px;
`;
export const Title = styled.div`
  width: 250px;
  height: 50px;
  margin-bottom: 0px;
  background-attachment: fixed;
  justify-content: center;
  flex-direction: row;
  text-align: center;
  border-right: 1px solid #091b20;
  border-radius: 0px 10px 10px 0px;
  display: flex;
  align-items: center;
  position: relative;
  -webkit-filter: drop-shadow(2px 0px 8px rgb(58, 44, 44));
  filter: drop-shadow(2px 0px 8px rgb(63, 49, 49));
  background: ${({ sidebar }) => (sidebar ? '#091b20' : '#027196')};
  transition: 350ms;
  z-index: 10;
`;
export let Container = styled.div`
  min-height: 100rem;
  align-content: flex-start;
  display: flex;
  flex-wrap: wrap;
  background: red;
  align-items: center;

  padding: 40px 10px 10px 10px;
  @media (max-width: 768px) {
    flex-direction: column;
    background-color: rebeccapurple;
  }
`;
export const SidebarNav = styled.nav`
  width: 250px;
  min-height: 100rem;
  justify-content: center;
  flex-wrap: wrap;
  position: relative;
  left: ${({ sidebar }) => (sidebar ? '0' : '-100%')};
  display: ${({ sidebar }) => (sidebar ? 'flow-root' : 'none')};
  /*transition: 350ms;*/
  z-index: 10;
  padding: 5px;
  border-top: 1px solid #ff8000;
  box-sizing: border-box;
  border-right: #ff8000 1px solid;
  float: left;
  background: #091b20;

`;
export let SidebarWrap = styled.div`
  padding-left: 15px;
  margin-top: 110px;
  position: fixed;
  z-index: 10;
`;
export let SidebarLink = styled.a`
  display: flex;
  color: rgb(136, 143, 145);
  justify-content: space-between;
  align-items: center;
  padding: 11px 20px;
  list-style: none;
  height: 60px;
  text-decoration: none;
  font-size: 18px;
  &:hover {
    width: 100%;
    background-color: #012734;
    cursor: pointer;
    color: #ffffff;
    border-radius: 30px 0px 0px 30px;
  }
`;
export const SidebarLabel = styled.span`
  margin-left: 12px;
`;
export const DropdownLink = styled.a`
  justify-content: space-evenly;
  display: flex;
  color: rgb(136, 143, 145);
  height: 60px;
  padding-left: 0.5rem;
  align-items: center;
  text-align: left;
  text-decoration: none;
  width: 190px;
  transition: max-height 0.15s ease-out;
  border-left: 1px solid white;
  border-radius: 15px;
  position: relative;
  left: 35px;
  transition: 350ms;
  z-index: 10;
  &:hover {
    background: #7c3a4f;
    cursor: pointer;
    border-radius: 5px;
    color: #ffffff;
  }
`;
export const UL = styled.ul`
  position: relative;
`;

export const Arrow = styled.span`
  font-size: 16px;
  text-align: center;
  opacity: 1;
  cursor: pointer;
  overflow: hidden;
  justify-content: flex-start;
  color: rgb(136, 143, 145);
  height: 20px;
  padding-left: 0.5rem;
  align-items: center;
  text-decoration: none;
  width: 190px;
  transition: max-height 0.15s ease-out;
  border-left: 0.5px solid white;
  border-radius: 5px;
  position: relative;
  left: 30px;
  z-index: 10;
  &:hover {
    background: #7c3a4f;
    cursor: pointer;
    border-radius: 5px;
    color: #ffffff;
    left: 10px;
  }
`;
export const Dashboar = styled.div`
  width: 220px;
  display: inline-flex;
  place-items: center start;
  text-justify: center;
  justify-content: space-evenly;
  padding-inline: 25px 90px;
  background-color: #027196;
  height: 35px;
  top: 60px;
  left: 24px;
  position: relative;
  font-size: 15px;
  border-radius: 30px 0px 0px 30px;
  color: aliceblue;
  box-sizing: border-box;
  border-bottom: #ff8000 1px solid;
  margin-top: 10px;
  padding-left: 0.5rem;
  position: fixed;
  z-index: 10;
  :hover {
    background: #7c3a4f;
    color: #ffffff;
    cursor: pointer;
  }
`;

export const Seachconten = styled.div`
  width: 300px;
  height: 30px;
  margin-left: 20px;
  background-color: #ff8000;
  display: flex;
  position: relative;
  border-radius: 50px;
  box-shadow: 4xp 4px 8px black;
  color: aqua;
`;

/*transform: translate(-50%, -50%);*/

export const Seach = styled.input`
  width: 90%;
  height: 30px;
  display: block;
  border-radius: 50px 0px 0px 50px;
  padding-left: 20px;
  font-size: 15px;
  outline: none;
  border: none;
  &::placeholder {
    color: rgb(136, 143, 145);
    font-family: console;
    font-size: 18px;
  }
  :hover {
    border: solid 0.5px #ff8000;
  }
`;

export const SeachBut = styled.button`
  position: absolute;
  background-color: #f7f8f9;
  right: -0px;
  top: 0px;
  width: 35px;
  height: 100%;
  border-radius: 0px 50px 50px 0px;
  font-size: 18px;
  outline: none;
  border: none;
  text-align: center;
  color: #ff8000;
  :hover {
    border: solid 0.5px #ff8000;
  }
`;

export const BodyLayouts = styled.div`
  background: #ff8000;
  width: 300px;
`;
