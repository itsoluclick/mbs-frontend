import Link from 'next/link';
import React, { useState } from 'react';

import {
  Arrow,
  DropdownLink,
  SidebarLabel,
  SidebarLink,
  UL,
} from '../SidebarStyled';
const SubMenu = ({ item }) => {
  const [menuA, setmenuA] = useState(false);
  const showmenuA = () => {
    setmenuA(!menuA);
  };

  const [menuB, setmenuB] = useState(false);
  const showmenuB = () => {
    setmenuB(!menuB);
  };

  const [menuvalue, setmenuvalue] = useState([]);
  const handleClicks = (value) => () => {
    setmenuvalue(value);
  };

  return (
    <div>
      <Link href={item.path}>
        <SidebarLink
          onClick={item.subNav && showmenuA}
          className={'SidebarLink'}
        >
          <div className={"section"}>
            {item.icon}
            <SidebarLabel className={'SidebarLabel'}>{item.title}</SidebarLabel>
          </div>
           <div >
            {item.subNav && menuA
              ? item.iconOpened
              : item.subNav
              ? item.iconClosed
              : null}
          </div>
        </SidebarLink>
      </Link>
      {menuA &&
        item.subNav.map((x, z) => {
          return (
            <div key={`${x}-${z}`}>
              <Link href={x.path}>
                <div className={'DropdownLink'} onClick={handleClicks(x.title)}>
                  <DropdownLink className="DropdownLink" onClick={showmenuB}>
                    {x.icon}
                    <SidebarLabel className={'SidebarLabel'}>
                      {x.title}
                    </SidebarLabel>
                    <div>
                      {(x.subMenus && menuB) ||
                        (x.subMenus ? item.iconClosed : null)}
                    </div>
                  </DropdownLink>
                </div>
              </Link>
              {typeof x.subMenus !== 'undefined' ? (
                <div>
                  {menuB &&
                    x.subMenus.map((y, s) => {
                      if (x.title == menuvalue) {
                        return (
                          <div key={`${y}-${s}`}>
                            <UL>
                              <Link href={y.path}>
                                <Arrow className={'submenu'}>
                                  {y.icon}
                                  <SidebarLabel className={'SidebarLabel'}>
                                    {y.title}
                                  </SidebarLabel>
                                </Arrow>
                              </Link>
                            </UL>
                          </div>
                        );
                      }
                    })}
                </div>
              ) : null}
            </div>
          );
        })}
    </div>
  );
};

export default SubMenu;
