import Dashboard from '@material-ui/icons/Dashboard';
import Image from 'next/image';
import React, { useState } from 'react';
import * as FaIcons from 'react-icons/fa';
import { IconContext } from 'react-icons/lib';

import CardLayoust  from '../cards/CardLayoust';
import { Container } from '../cards/Layoust';
import { SidebarData } from './data/SideData';
import SubMenu from './parts/SubMenu';
import {
  Dashboar,
  Header,
  Nav,
  NavIcon,
  Seach,
  SeachBut,
  Seachconten,
  SidebarNav,
  SidebarWrap,
  Title,
} from './SidebarStyled';

const Sidebar = () => {
  const [sidebar, setSidebar] = useState(false);

  const [filter, setFilter] = useState('');
  const showSidebar = () => setSidebar(!sidebar);
  function handleClick(event) {
    event.preventDefault();
    event.stopPropagation();
  }
  return (
    <>
      <Nav className="Nav">
        <Title sidebar={sidebar} className="sidebar">
          <div id={'imagetitle'}>
            <Image width={100} height={100} src="/logo.jpg" alt="mbs"></Image>
          </div>
          <div>
            <Header>{'Plaza Lama'}</Header>
          </div>
        </Title>

        <NavIcon sidebar={sidebar} className={'NavIcon'}>
          <FaIcons.FaBars onClick={showSidebar} />
        </NavIcon>
        <Seachconten>
          <Seach
            type={'text'}
            placeholder={'Search.....'}
            name={'search'}
            maxLength={33}
            onChange={(e) => setFilter(e.target.value)}
          ></Seach>
          <SeachBut type={'submit'} className={'button'}>
            <FaIcons.FaSearch />
          </SeachBut>
        </Seachconten>
        <div className={'perfil_user'}>
          <di className={'image'}>
            <Image
              width={100}
              height={100}
              src="/avatar1.jpg"
              alt="mbs"
            ></Image>
          </di>
        </div>
        <h5>{'hola mundo '}</h5>
      </Nav>
      <IconContext.Provider value={{ color: '#fff' }}>
        <SidebarNav
          sidebar={sidebar}
          className={'SidebarNav'}
          onClick={handleClick}
        >
          <div >
            <Dashboar>
              <Dashboard />
             
              {'Dashboard'}
            </Dashboar>.
            
            <SidebarWrap className={'SidebarWrap'}>
              
              {SidebarData.map((item, index) => {
                return (
                  <div key={`${item}-${index}`}>
                    <SubMenu item={item} className={'SubMenu'} />
                  </div>
                );
              })}
            </SidebarWrap>
          </div>
        </SidebarNav>


      </IconContext.Provider>
    </>
  );
};

export default Sidebar;
