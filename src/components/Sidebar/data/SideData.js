import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import Dashboard from '@material-ui/icons/Dashboard';
import Person from '@material-ui/icons/Person';
import React from 'react';
import * as BiIcons from 'react-icons/bi';
import * as CgIcons from 'react-icons/cg';
import * as FaIcons from 'react-icons/fa';
import * as GigIcon from 'react-icons/gi';
import * as GogIcon from 'react-icons/go';
import * as IoIcons from 'react-icons/io';
import * as RiIcons from 'react-icons/ri';
import * as TiIcons from 'react-icons/ti';
export const SidebarData = [
  {
    title: 'Inventario',
    path: '/',
    icon: <AssignmentTurnedInIcon />,
    iconClosed: <RiIcons.RiArrowDownSFill />,
    iconOpened: <RiIcons.RiArrowUpSFill />,

    subNav: [
      {
        title: 'Almacenes',
        path: '/services/services3',
        icon: <FaIcons.FaWarehouse />,
        subMenus: [],
      },
      {
        title: 'Productos',
        path: '/',
        icon: <GigIcon.GiBoxUnpacking />,
        Name: 'sub-nav',

        subMenus: [
          {
            title: 'Lineas',
            path: '/',
            icon: <IoIcons.IoIosPaper />,
            cName: 'sub-nav',
          },
          {
            title: 'Fabricante',
            path: '/services/services3',
            icon: <IoIcons.IoIosPaper />,
          },
          {
            title: 'Marca',
            path: '/services/services3',
            icon: <IoIcons.IoIosPaper />,
          },
        ],
      },

      {
        title: 'Grupo',
        path: '/',
        icon: <IoIcons.IoIosPaper />,
        subMenus: [
          {
            title: 'Sub Grupo',
            path: '/services/services2',
            icon: <IoIcons.IoIosPaper />,
            cName: 'sub-nav',
          },
        ],
      },
      {
        title: 'Control de Inv',
        path: '/services/services3',
        icon: <GogIcon.GoChecklist />,
      },
      {
        title: 'Movimiento Inv',
        path: '/services/services3',
        icon: <FaIcons.FaLuggageCart />,
      },
      {
        title: 'Inv Fco Fecha',
        path: '/services/services3',
        icon: <FaIcons.FaRegCalendarAlt />,
      },
    ],
  },
  {
    title: 'Cliente',
    path: '/',
    icon: <Person />,
    iconClosed: <RiIcons.RiArrowDownSFill />,
    iconOpened: <RiIcons.RiArrowUpSFill />,

    subNav: [
      {
        title: 'Contatos Cliente',
        path: '/about-us/aim',
        icon: <TiIcons.TiContacts />,
        subMenus: [],
      },
      {
        title: 'Cliente Concepto',
        path: '/about-us/vision',
        icon: <IoIcons.IoIosPaper />,
        subMenus: [],
      },
      {
        title: 'Cliente Preferencial',
        path: '/about-us/vision',
        icon: <BiIcons.BiStar />,
        subMenus: [],
      },
      {
        title: 'Cuenta x Cobar Detalle',
        path: '/about-us/vision',
        icon: <FaIcons.FaFileInvoice />,
        subMenus: [],
      },
    ],
  },
  {
    title: 'Proveedore',
    path: '/',
    icon: <AddShoppingCartIcon />,
    iconClosed: <RiIcons.RiArrowDownSFill />,
    iconOpened: <RiIcons.RiArrowUpSFill />,

    subNav: [
      {
        title: 'Empresa',
        path: '/events/events1',
        icon: <IoIcons.IoIosBusiness />,
        subMenus: [],
      },
      {
        title: 'Proveedores Concepto',
        path: '/events/events2',
        icon: <IoIcons.IoIosPaper />,
        subMenus: [],
      },
    ],
  },
  {
    title: 'Venta',
    path: '/',
    icon: <AddShoppingCartIcon />,
    iconClosed: <RiIcons.RiArrowDownSFill />,
    iconOpened: <RiIcons.RiArrowUpSFill />,
    subNav: [
      {
        title: 'Caja',
        path: '/events/events1',
        icon: <FaIcons.FaCashRegister />,
        subMenus: [],
      },
      {
        title: 'Documentos',
        path: '/events/events2',
        icon: <CgIcons.CgFileDocument />,
        subMenus: [
          {
            title: 'Documento Detalle',
            path: '/services/services2',
            icon: <IoIcons.IoIosPaper />,
            Name: 'sub-nav',
          },
        ],
      },
    ],
  },
  {
    title: 'Support',
    path: '/support',
    icon: <IoIcons.IoMdHelpCircle />,
  },
];
