import styled from 'styled-components';
export const Card = styled.div`
  background-color: white;
  border-radius: 1rem;
  box-shadow: inset 0 -0em 0em rgba(0, 0, 0, 0.1), 0 0 0 2px rgb(255, 255, 300),
    0.2em 0.2em 0.2em rgba(0, 0, 0, 0.3);
  display: flex;
  margin: 1.5rem;
  overflow: hidden
  width: 20rem;
  height: 10rem;
  object-fit: contain;
`;

export const Cardmini = styled.div`
  width: 14rem;
  height: 6rem;
  background-color: #ff8000;
  border-radius: 1rem;
  box-shadow: inset 0 -0em 0em rgba(0, 0, 0, 0.1), 0 0 0 2px rgb(255, 255, 300),
    0.1em 0.1em 0.1em rgba(0, 0, 0, 0.3);
  margin: 1.5rem;
  position: sticky;
  align-items: center;
`;
