/* eslint-disable prettier/prettier */
import React, { useState } from 'react';

import {  Card, Cardmini } from '../cards/CardStyles';
import {Container} from '../cards/Layoust';
import { SidebarData } from '../Sidebar/data/SideData';

function CardLayoust( ) {
 
  const [cardss, setcards] = useState(false);
  const showcards = () => {
    setcards(!cardss);

  };
  
const [subcardss, setsubcardss] = useState([]);
  const handleClicks = (value) => () => {
 
      setsubcardss(value)   
     
     
    };

function handleClick(event) {
    event.preventDefault();
   event.stopPropagation();
  
}      
    return (
     <>
     {SidebarData.map((item, index)=>{
       return(<div  key={`${item}-${index}`}> 
              <Card onClick={handleClicks(item.title)}>
                  <div className={'left-column backgroun-left'}  >
                <h4>{item.title}</h4>
                <hr/>
                <i>{item.icon}</i>
              </div>
              <div className={'right-column'} >
                <h4>{item.title}</h4>
                <i>{item.icon}</i>             
                
              </div>
              <button className={'buttonm'} key={item} onClick={showcards}  >Detalle</button>
              
              </Card>
                  {typeof item.subNav !== 'undefined' ? (
                <div > 
               {  cardss &&           
                item.subNav.map((x,y)=>{
                 if (item.title == subcardss) {                                   
                 return(
                    <div  key={`${x}-${y}`}>
                     <Cardmini>

                      <div className={'left-columnmimi backgroun-left'}  >
                <h4>{item.title}</h4>
                <hr/>
                <i>{item.icon}</i>
              </div>
              <div className={'right-columnmimi'} >
                <h4>{item.title}</h4>
                <i>{item.icon}</i>             
                
              </div>


                     </Cardmini>
                                      
                      </div>
                                    
                
               )}})}             
                
                </div>
              ) : null}

               </div>)
               
     })}
        
      </>
    );
}

export default CardLayoust;
